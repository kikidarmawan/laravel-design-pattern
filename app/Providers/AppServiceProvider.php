<?php

namespace App\Providers;

use App\Interfaces\Api\BajuInterface;
use App\Interfaces\Api\PostInterface;
use App\Interfaces\Api\UserInterface;
use App\Repositories\BajuRepository;
use App\Repositories\PostRepository;
use App\Repositories\UserRepository;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(PostInterface::class, PostRepository::class);
        $this->app->bind(UserInterface::class, UserRepository::class);
        $this->app->bind(BajuInterface::class, BajuRepository::class);
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
