<?php

namespace App\Repositories;

use App\Interfaces\Api\PostInterface;
use App\Models\Post;

class PostRepository implements PostInterface
{

    public $postModel;

    public function __construct(Post $postModel)
    {
        $this->postModel = $postModel;
    }

    public function getAll()
    {
        return $this->postModel->all();
    }

    public function store($request)
    {
    }

    public function update($request, $id)
    {
    }

    public function show($id)
    {
    }

    public function destroy($id)
    {
    }

}
