<?php

namespace App\Http\Resources\Api;

use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class BajuResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id_baju'        => $this->id,
            'nama_baju'      => $this->nama,
            'harga_baju'     => $this->harga,
            'tanggal_dibuat' => Carbon::parse($this->created_at)->format('d F Y H:i'),
            'user'           => new UserResource($this->whenLoaded('user'))
        ];
    }
}
