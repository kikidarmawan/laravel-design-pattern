<?php

namespace App\Repositories;

use App\Interfaces\Api\UserInterface;
use App\Models\User;

class UserRepository implements UserInterface
{

    public $userModel;

    public function __construct(User $userModel)
    {
        $this->userModel = $userModel;
    }

    public function getAll()
    {
        return $this->userModel->with(['posts', 'baju'])->get();
    }

    public function store($request)
    {
    }

    public function update($request, $id)
    {
    }

    public function show($id)
    {
    }

    public function destroy($id)
    {
    }

}
