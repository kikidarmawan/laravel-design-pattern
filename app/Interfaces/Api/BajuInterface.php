<?php

namespace App\Interfaces\Api;

interface BajuInterface
{
    public function getAll();
    public function store($request);
    public function update($request, $id);
    public function show($id);
    public function destroy($id);
}
