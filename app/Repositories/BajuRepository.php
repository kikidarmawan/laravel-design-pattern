<?php

namespace App\Repositories;

use App\Interfaces\Api\BajuInterface;
use App\Models\User;
use App\Models\Baju;

class BajuRepository implements BajuInterface
{
    public $baju;

    public function __construct(Baju $baju)
    {
        $this->baju = $baju;
    }

    public function getAll()
    {
        return $this->baju->with(['user'])->get();
    }

    public function store($request)
    {

        $baju = $this->baju->create($request->all());

        return $baju;
    }

    public function update($request, $id)
    {
    }

    public function show($id)
    {
    }

    public function destroy($id)
    {
    }
}
