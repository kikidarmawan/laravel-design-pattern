<?php

namespace App\Http\Controllers;

use App\Http\Resources\Api\UserResource;
use App\Interfaces\Api\UserInterface;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class UserController extends Controller
{
    protected $user;

    public function __construct(UserInterface $user)
    {
        $this->user = $user;
    }

    public function index(): JsonResponse
    {
        try {
            $data = $this->user->getAll();

            return response()->json([
                'status' => true,
                'data'   => UserResource::collection($data)
            ], 200);

        } catch (\Throwable $th) {
            return response()->json([
                'status'  => false,
                'message' => $th->getMessage()
            ], 500);
        }
    }
}
